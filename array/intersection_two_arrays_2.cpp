#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
}

class Solution {
public:
    std::vector<int> intersect(std::vector<int>& nums1, std::vector<int>& nums2) {
        std::sort(nums1.begin(), nums1.end());
        std::sort(nums2.begin(), nums2.end());

        std::vector<int> output;
        std::set_intersection(nums1.begin(), nums1.end(),
                              nums2.begin(), nums2.end(),
                              std::back_inserter(output));

        return output;
    }
};

int main() {
    Solution solution;
    std::vector<int> arrayA {4, 9, 5};
    std::vector<int> arrayB {9, 4, 9, 8, 4};

    std::vector<int> result = solution.intersect(arrayA, arrayB);

    printArray(result);

    return 0;
}
