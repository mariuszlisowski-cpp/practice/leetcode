#include <algorithm>
#include <iostream>
#include <vector>

class Solution {
public:
    void rotate(std::vector<int>& nums, int k) {
        if (!(k % nums.size())) {
            std::cout << "nothing to do..." << std::endl;
            return;
        }
        std::cout << "rotating right for " << k % nums.size() << " place(s)" << std::endl;
        std::rotate(nums.rbegin(), nums.rbegin() + (k % nums.size()), nums.rend());
    }
};

int main() {
    Solution solution;
    std::vector<int> array {1, 2, 3, 4, 5, 6, 7};

    solution.rotate(array, 22);
    
    for (auto &&i : array) {
        std::cout << i << ' ';
    }
    std::cout << std::endl;

    return 0;
}
