#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
}

class Solution {
public:
    std::vector<int> moveZeroes(std::vector<int>& nums) {
        if (nums.size() > 1) {
            size_t counter{1};
            for (auto it = nums.begin(); it != nums.end(); /* no op */) {
                if (*it == 0) {
                    it = nums.erase(it);
                    ++counter;
                } else {
                    it++;
                }
            }
            while (--counter) {
                nums.emplace_back(0);
            }
        }
        
        return nums;
    }

};

int main() {
    Solution solution;
    std::vector<int> array {0, 1, 0, 3, 12, 0}; // 1, 3, 12, 0, 0, 0

    std::vector<int> result = solution.moveZeroes(array);

    printArray(result);

    return 0;
}
