#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

template <typename T>
void printArray(std::vector<T> vector) {
    for (T& array : vector) {
        for (auto& value : array) {
            std::cout << value << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

class Solution {
public:
    void rotate(std::vector<std::vector<int>>& matrix) {
        int n = matrix.size();
        for (int i = 0; i < (n + 1) / 2; i ++) {
            for (int j = 0; j < n / 2; j++) {
                int temp = matrix[n - 1 - j][i];
                matrix[n - 1 - j][i] = matrix[n - 1 - i][n - j - 1];
                matrix[n - 1 - i][n - j - 1] = matrix[j][n - 1 -i];
                matrix[j][n - 1 - i] = matrix[i][j];
                matrix[i][j] = temp;
            }
        }
    }
};

// CS solution
void rotate(std::vector<std::vector<int>>& matrix) {
    const auto size = matrix.size();
    std::reverse(matrix.begin(), matrix.end());
 
    for(int i = 0; i < size; i++) {
        for (int j = 0; j < i; j++) {
            std::swap(matrix[i][j], matrix[j][i]);
        }
    }
}

int main() {
    Solution solution;
    std::vector<std::vector<int>> matrix = {
        { 5,  1,  9, 11},
        { 2,  4,  8, 10},
        {13,  3,  6,  7},
        {15, 14, 12, 16}
    }; // has to be a rectangle

    solution.rotate(matrix);
    printArray(matrix);

    rotate(matrix);
    printArray(matrix);

    return 0;
}
