#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_set>

class Solution {
public:
    bool containsDuplicate(std::vector<int>& nums) {
        std::unordered_set<int> uniques(nums.begin(), nums.end());

        return uniques.size() != nums.size();
    }
};

int main() {
    Solution solution;
    std::vector<int> array {1, 2, 3, 4, 5, 6, 7, 1};

    std::cout << std::boolalpha << solution.containsDuplicate(array) << std::endl;

    for (auto&& i : array) {
        std::cout << i << ' ';
    }

    return 0;
}
