#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

class Solution {
public:
    int singleNumberA(std::vector<int>& nums) {
        std::unordered_map<int, int> counts;
        
        for (size_t i = 0; i < nums.size(); i++) {
            ++counts[nums[i]];
        }
        
        auto it = std::find_if(counts.begin(), counts.end(),
                     [](std::pair<int, int> pair){
                         return pair.second == 1;
                     });

        return it == counts.end() ? 0 : (*it).first;
    }

    int singleNumberB(std::vector<int>& nums) {
        std::unordered_map<int, int> counts;
        
        for (size_t i = 0; i < nums.size(); i++) {
            ++counts[nums[i]];
        }
        
        for (std::pair<int, int> pair : counts) {
            std::cout << pair.first << " : " << pair.second << std::endl;
            if (pair.second == 1) {
                return pair.first;
            }
        }
        
        return 0;
    }

    

    int singleNumberC(std::vector<int>& nums) {
        int a = 0;
        for (int i : nums) {
        a ^= i;
        }
        return a;
    }
};


int main() {
    Solution solution;
    std::vector<int> array {4, 1, 2, 1, 2}; // 4

    std::cout << solution.singleNumberC(array) << std::endl;

    return 0;
}
