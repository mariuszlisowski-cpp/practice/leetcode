// parentheses validation 
// ~ ignores other characters between

#include <iostream>
#include <stack>
#include <string>

bool areParenthesesValid(const std::string& str) {
    std::stack<char> stack{};
    stack.push('\x00'); // cannot be empty
    for (char ch : str) {
        // std::cout << "Char: " << ch << ' ';

        if (ch == '(' || ch == '[' || ch == '{') {
            stack.push(ch);
            // std::cout << ", pushed, size: " << stack.size() << std::endl;
        } else if ((ch == ')' && stack.top() == '(') ||
                   (ch == ']' && stack.top() == '[') ||
                   (ch == '}' && stack.top() == '{')) {
            stack.pop();
            // std::cout << ", popped, size: " << stack.size() << std::endl;
        } else {
            if (ch == ')' || ch == ']' || ch == '}') {
                stack.push(ch);
                // std::cout << ", pushed, size: " << stack.size() << std::endl;
            } else {
                // std::cout << ", ignored" << std::endl;
            }
        }
    }

    return stack.size() == 1; // first inserted zero remains
}

int main() {
    std::string strA = "if (true) { retrun array[index]; }";
    std::string strB = "{if (true) { retrun array[index]; }";
    
    std::cout << (areParenthesesValid(strA) ? "Valid" : "Invalid") << std::endl;
    std::cout << (areParenthesesValid(strB) ? "Valid" : "Invalid") << std::endl;
    
    return 0;
}
// invalid:
// "([){}])";
// "([)]";
// "[(])";
// ")[()]"
// "[(])";

// valid:
// "([{}])";
// "(([[{}]]))"
// "([])";
// "[()]{}"
// "({})[]{}";
// "(){}[]";
// "(a){b}[c]";

