#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>
#include <string>

class Solution {
public:
    int strStr(std::string haystack, std::string needle) {
        size_t result{};
        if ((result = haystack.find(needle)) != std::string::npos) {
            return result;
        } else {
            return -1;
        }
    }
};

int main() {
    Solution solution;

    std::string haystack = "hello", 
                needle = "ll";

    std::cout << solution.strStr(haystack, needle);

    return 0;
}
