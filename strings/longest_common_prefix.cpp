#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

class Solution {
public:
    std::string longestCommonPrefixMine(std::vector<std::string>& strs) {
        if (strs.empty()) {
            return "";
        }
        if (strs.size() == 1) {
            return strs.front();
        }
        size_t i{}, pos{};
        std::stringstream ss{""};
        bool diff{};
        while (true) {
            for (size_t i = 0; i < strs.size() - 1; i++) {
                if (strs.at(i) == "" || strs.at(i + 1) == "") {
                    return "";
                }
                if (pos <= strs.at(i).length() - 1 &&
                    pos <= strs.at(i + 1).length() - 1 )
                {
                    if (strs.at(i)[pos] != strs.at(i + 1)[pos]) {
                        diff = true;
                    }
                } else {
                    diff = true;
                }
            }
            if (diff) {
                return ss.str();
            } else {
                ss << strs.at(i)[pos];
                ++pos;
            }
        }
    }

    // LeetCode solution
    std::string longestCommonPrefixLeet(std::vector<std::string>& strs) {
        if (strs.size() == 0) {
            return "";
        }
        std::string prefix = strs.at(0);
        for (int i = 1; i < strs.size(); i++) {
            while (strs.at(i).find(prefix) != 0) {
                prefix = prefix.substr(0, prefix.length() - 1);
                if (prefix.empty()) {
                    return "";
                }
            }        
        }

        return prefix;
    }

    // fastest
    std::string longestCommonPrefix(std::vector<std::string>& strs) {
        if (strs.empty()) {
            return "";
        }
        int min = strs[0].length();
        for (int i = 1; i < strs.size(); i++) {
            if (min > strs[i].length()) {
                min = strs[i].length();
            }
            for (int j = 0; j < min; j++) {
                if (strs[i][j] != strs[i - 1][j]) {
                    min = j;
                    break;
                }
            }
        }
     
        return strs[0].substr(0, min);
    }
};

int main() {
    Solution solution;

    std::vector<std::string> vecA{"flower", "flow", "flight"};  // "fl"
    std::vector<std::string> vecB{"dog", "racecar", "car"};     // no common prefix ("")
    std::vector<std::string> vecC{"flow", "flow", "flow"};      // "flow"
    std::vector<std::string> vecD{"dog"};                       // "dog"
    std::vector<std::string> vecE{"", ""};                      // ""
    std::vector<std::string> vecF{""};                          // ""
    std::vector<std::string> vecG{};                            // ""

    std::cout << "> " << solution.longestCommonPrefix(vecA) << std::endl;
    std::cout << "> " << solution.longestCommonPrefix(vecB) << std::endl;
    std::cout << "> " << solution.longestCommonPrefix(vecC) << std::endl;
    std::cout << "> " << solution.longestCommonPrefix(vecD) << std::endl;
    std::cout << "> " << solution.longestCommonPrefix(vecE) << std::endl;
    std::cout << "> " << solution.longestCommonPrefix(vecF) << std::endl;
    std::cout << "> " << solution.longestCommonPrefix(vecG) << std::endl;

    return 0;
}
