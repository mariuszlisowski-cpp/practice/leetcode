#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << static_cast<char>(value);
    }
    std::cout << std::endl;
}

class Solution {
public:
     int firstUniqChar(std::string s) {
        std::unordered_map<char, size_t> map;
        for (char ch : s) {
            map[ch]++;
        }
        for (size_t i = 0; i < s.size(); ++i) {
            if (map[s[i]] == 1) {
                return i;
            }
        }

        return -1;
    }    
};

int main() {
    Solution solution;
    
    int index = solution.firstUniqChar("loveleetcode"); // 2 (v)
    std::cout << index << std::endl;

    return 0;
}
