#include <initializer_list>
#include <iostream>

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
  public:
    bool hasCycle(ListNode *head) {
        if (!head) {
            return false;
        }
        ListNode* slow = head;
        ListNode* fast = head->next;
        while (fast && fast->next && slow) {
            if (slow == fast) {
                return true;
            }
            slow = slow->next;
            fast = fast->next->next;
        }

        return false;
    }

    void make_cyclic(ListNode* head) {
        if (!head) {
            return;
        }
        ListNode* curr = head;
        while (curr->next) {
            curr = curr->next;
        }
        curr->next = head;
    }

    ListNode* populate_list(std::initializer_list<int> list) {
        ListNode* head;
        ListNode* last;
        for (auto value : list) {
            ListNode* node = new ListNode(value);
            if (!head) {
                head = node;
                last = head;
            } else {
                last->next = node;
                last = last->next;
            }
        }

        return head;
    }

    void print_nodes(ListNode* head) {
        while (head != nullptr) {
            std::cout << head->val << ' ';
            head = head->next;
        }
        std::cout << std::endl;
    }
};

int main() {
    Solution solution;

    ListNode* head = solution.populate_list({1, 2, 3, 4, 5});
    std::cout << (solution.hasCycle(head) ? "cycle" : "no cycle") << std::endl;

    solution.print_nodes(head);
    
    solution.make_cyclic(head);
    std::cout << (solution.hasCycle(head) ? "cycle" : "no cycle") << std::endl;

    return 0;
}
