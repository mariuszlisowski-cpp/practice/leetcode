#include <initializer_list>
#include <iostream>

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

ListNode* populate_list(std::initializer_list<int> list) {
    ListNode* head{};
    ListNode* last{};
    for (auto value : list) {
        ListNode* node = new ListNode(value);
        if (!head) {
            head = node;
            last = head;
        } else {
            last->next = node;
            last = last->next;
        }
    }

    return head;
}

void print_nodes(ListNode* head) {
    while (head) {
        std::cout << head->val << ' ';
        head = head->next;
    }
    std::cout << std::endl;
}

void move_node(ListNode** destRef, ListNode** sourceRef) {
    /* the front source node */
    ListNode* newNode = *sourceRef;
    assert(newNode != NULL);
    /* Advance the source pointer */
    *sourceRef = newNode->next;
    /* Link the old dest off the new node */
    newNode->next = *destRef;
    /* Move dest to point to the new node */
    *destRef = newNode;
}

ListNode* merge_two_lists(ListNode* l1, ListNode* l2) {
    ListNode* result = NULL;
    /* point to the last result pointer */
    ListNode** lastPtrRef = &result;

    while (true) {
        if (l1 == NULL) {
            *lastPtrRef = l2;
            break;
        } else if (l2 == NULL) {
            *lastPtrRef = l1;
            break;
        }
        if (l1->val <= l2->val) {
            move_node(lastPtrRef, &l1);
        } else {
            move_node(lastPtrRef, &l2);
        }
        /* tricky: advance to point to the next ".next" field */
        lastPtrRef = &((*lastPtrRef)->next);
    }
    return (result);
}

int main() {
    ListNode* headA = populate_list({1, 3, 5, 7});      // sorted
    print_nodes(headA);

    ListNode* headB = populate_list({0, 4, 6, 8});      // sorted
    print_nodes(headB);

    ListNode* merged = merge_two_lists(headA, headB);   // also sorted
    print_nodes(merged);

    return 0;
}
